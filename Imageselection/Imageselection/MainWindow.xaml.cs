﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Imageselection
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Image> Img = new List<Image>();
        public MainWindow()
        {
            // create instance for all image
            Img.Add(new Image("Apple", "Images/apple.jpg"));
            Img.Add(new Image("Orange", "Images/orange.jpg"));
            Img.Add(new Image("Grapes", "Images/grapes.jpg"));
            Img.Add(new Image("Pomegranate", "Images/pomegranate.jpg"));
            Img.Add(new Image("Watermelon", "Images/watermelon.jpg"));
            DataContext =Img;
            InitializeComponent();
        }
        /// <summary>
        /// Display image icon and name for selected image
        /// </summary>
        /// <param name="sender">Control elementt</param>
        /// <param name="e">event argument</param>
        public void ImageSelection(Object sender,RoutedEventArgs e)
        {
            Image i = (Image)ImageList.SelectedItem;
            imgname.Content = i.ImageName;
            Uri imageUri = new Uri(i.ImagePath, UriKind.Relative);
            BitmapImage imageBitmap = new BitmapImage(imageUri);
            img.Source = imageBitmap;
        }
    }
    /// <summary>
    /// store and retrieve Image name and path
    /// </summary>
    public class Image
    {
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
        /// <summary>
        /// Set name and path for image instance
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="Path"></param>
        public Image(String Name,String Path)
        {
            ImageName = Name;
            ImagePath = Path;
        }
    }
}
